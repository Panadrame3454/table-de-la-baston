#!/bin/python3

# -*- coding: utf-8 -*-
# @Author: brunomaxime
# @Date:   2022-09-09 14:52:25
# @Last Modified by:   brunomaxime
# @Last Modified time: 2022-09-09 22:03:02


from ruamel.yaml import YAML, RoundTripDumper
from ruamel.yaml.comments import CommentedMap
import ruamel


PATH = "./petits_numeros.yml"

def check(dic):
    if isinstance(dic, dict):
        elt = dic.items()
    else:
        elt = enumerate(dic)
    for k,v in elt:
        if isinstance(v,dict) or isinstance(v,list):
            check(v)
        elif not isinstance(v, str) and not isinstance(v, int):
            print(type(v))
            print(v)
        elif isinstance(v, str):
            try:
                dic[k] = int(v)
            except ValueError as _:
                n = len(v)
                if n > 0 and v[n-1] == '\n':
                    dic[k] = v[:n-1]

def rec():
    txt = input()
    res = []
    while len(txt) > 0:
        if ':' in txt:
            if isinstance(res, list):
                if res == []:
                    res = dict()
                else:
                    with open(PATH, 'w', encoding="utf-8") as file:
                        yaml.dump(content, file)
                    raise ValueError
            key = txt[:-1]
            try:
                key = int(key)
            except ValueError:
                pass
            res[key] = rec()
        else:
            if isinstance(res, list):
                res.append(txt)
            else:
                with open(PATH, 'w', encoding="utf-8") as file:
                    yaml.dump(content, file)
                raise ValueError
        txt = input()
    if isinstance(res, list) and len(res) == 1:
        return res[0]
    else:
        return res

yaml = YAML()
yaml.indent(mapping=2, sequence=4, offset=2)
yaml.width = 80
yaml.explicit_start = True

with open(PATH, 'r', encoding="utf-8") as file:
    content = yaml.load(file)

check(content)

defect = []

for k,v in content.items():
    try:
        if ':' in v["description"]:
            defect.append(k)
    except KeyError:
        print("Error : " + str(k))

print(defect)
print(str(len(defect)) + " left")
x = int(input())

while x != 0 :
    print(content[x])
    i = 0
    res = rec()
    print(res)
    val = int(input())
    while val != 0 :
        res = rec()
        print(res)
        val = int(input())
    content[x] = res
    n_defect = []
    for i in defect :
        if i != x:
            n_defect.append(i)
    defect = n_defect
    print(defect)
    print(str(len(defect)) + " left")
    x = int(input())

with open(PATH, 'w', encoding="utf-8") as file:
    yaml.dump(content, file)

# print(content)
